
const msRestAzure = require("ms-rest-azure");
const AuthorizationManagementClient = require("azure-arm-authorization");

/**
 *
 * @param {string} clientId
 * @param {string} secret
 * @param {string} tenant
 * @param {string} subscriptionId
 * @returns Promise<RoleDefinitionListResult>
 */

exports.checkCustomRoles = (clientId, secret, tenant, subscriptionId) => {
    return msRestAzure
        .loginWithServicePrincipalSecret(clientId, secret, tenant)
        .then((credentials) => {
            const client = new AuthorizationManagementClient(credentials, subscriptionId);
            return client
                .roleDefinitions
                .list(`subscriptions/${subscriptionId}`)
                .then((result) => {
                    result.forEach((role) => {
                        if (role.roleType === 'CustomRole') {
                            throw Error(`Custom Role created. Role name: ${role.roleName}`)
                        }
                    });
                    // Some business login here?
                    return result;
                });
        })
        .catch((err) => {
            throw err;
        });
}