### Installation

`npm install azure-test-task`

### Usage

```
const azureTest = require('azure-test-task');

const clientId = 'bf249d1c-ca08-41b0-bd87-fde78f050916';
const secret = 'yoRw2Wk5L';
const tenant = 'a3388346-8d2c-467f-925a-9c8dc21b9116';
const subscriptionId = '5249d04b-9dab-4a46-9cbe-2920154836ec';

azureTest.checkCustomRoles(clientId, secret, tenant, subscriptionId).then((result) => {
    console.log('result', result);
}).catch((err)=> {
    console.log(err)
});
```

### Notes
Module will always return Promise object. Either rejected or fullfilled. Error object will contain message about Custom Role and it's name. Fullfilled result will contain list of all subscription roles
You can use my subscription for testing. Also you can find JSON that you can use to define Custom Role via Azure CLI. From the package folder:

`az role definition create --role-definition ./test-custom-role.json`